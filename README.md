## Ruleta frontend

**Importante, tener corriendo el backend y haber hecho los pasos mencionados** 

1. Tener las siquientes instalaciones 

- npm y node js
`sudo apt install nodejs`
- Angular cli
`sudo npm install -g @angular/cli`

2. Instalar dependencias npm con `npm i`  & correr el proyecto con `ng serve`

3. Al iniciar sesion el username por defecto es **admin** y la contraseña **1234** para probar toda la funcionalidad

Listo tienes el proyecto listo para usar.
