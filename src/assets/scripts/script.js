const black = "#373737"
const red = "#ac0800"
const green = "#5a9216"
const btn_play = document.getElementById('btn-play')

let theWheel = new Winwheel({
    'numSegments': 31,
    "textFontSize": 6,
    'innerRadius': 10,
    'textAlignment': 'center',         // Set alignment: inner, outer, center.

    'segments':
        [
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
            { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white' },
            { 'fillStyle': green, 'text': 'v', 'textFillStyle': 'white' },

        ],
    'animation':                   // Note animation properties passed in constructor parameters.
    {
        'type': 'spinToStop',
        'duration': 5,
        'spins': 2,
        // se ejecuta cuando termina de girar
        'callbackFinished': 'alertPrize()',
        // Durante la animación es necesario llamar a drawTriangle () para volver a dibujar el puntero en cada cuadro.
        'callbackAfter': 'drawTriangle()'
    }
});


function playGame () {
    const { color, saldo, confirm} = JSON.parse(localStorage.getItem('eleccionUser'))

    if (color && saldo && confirm) {
        btn_play.disabled = true;
        theWheel.startAnimation();
        return
    } 
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Llena todos los campos y confirma tu eleccion!!',
    })

}


function alertPrize() {
    // Call getIndicatedSegment() function to return pointer to the segment pointed to on win.
    let winningSegment = theWheel.getIndicatedSegment();
    let colorVista = ''

    // GET INFO USER OF localStorage
    const eleccionObj = JSON.parse(localStorage.getItem('eleccionUser'))
    eleccionObj.devuelveInformacion = true
    
    switch (eleccionObj.color) {
        case 'r':
            colorVista = "Rojo"
            break
        case 'n':
            colorVista = "Negro"
            break
        case 'v':
            colorVista = "Verde"
    }

    if (eleccionObj.color == winningSegment.text) {
        if (colorVista == "Verde") {
            eleccionObj.apuntoVerde = true
            Swal.fire({
                icon: 'success',
                title: 'Felicidades ganastes el premio mayor!',
                text: `Felicidades has acertado el color ${colorVista} !!`,
            })
            
        } else {
            Swal.fire({
                icon: 'success',
                title: 'Hey!',
                text: `Felicidades has acertado el color ${colorVista} !!`,
            })
        }
        // verificamos que haya ganado
        eleccionObj.gano = true
        
    }

    else {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Los colores no coinciden!!',
        })
        eleccionObj.gano = false
    }  

    localStorage.setItem('eleccionUser', JSON.stringify(eleccionObj))

    // espera 2s para que refresque la pagina
    setTimeout(() => {
        location.reload();
    }, 2000)
}

// Function to draw pointer using code (like in a previous tutorial).
drawTriangle();

function drawTriangle() {
    // Get the canvas context the wheel uses.
    let ctx = theWheel.ctx;

    ctx.beginPath();              // Begin path.
    ctx.moveTo(170, 5);           // Move to initial position.
    ctx.stroke();                 // Complete the path by stroking (draw lines).
    ctx.fill();                   // Then fill.
}

function resetWheel() {
    theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
    theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
    theWheel.draw();                // Call draw to render changes to the wheel.
    btn_play.disabled = false;
    wheelSpinning = true;          // Reset to false to power buttons and spin can be clicked again.
}





