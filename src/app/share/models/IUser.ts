
export interface IUser {
     
     id?: number,
     nombre: string,
     estado: boolean,
     fecha_creacion?: Date,
     saldo: number,
     username: string,
     password: string,
     rolsUsers: [
          {
               id: number,
               rol?: string,
          }
     ],
     authorities?: string[],

 
}


