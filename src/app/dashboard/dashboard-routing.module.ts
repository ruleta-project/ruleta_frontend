import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RuletaComponent } from '../modules/ruleta/ruleta.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';

//Inyect of modules neccesary
const routes: Routes = [
  {
    path: '',
    component: MainDashboardComponent,
    children: [
      {
        path: '',
        component: RuletaComponent
      },
      {
        path: 'detail-user',
        loadChildren: () => import('../modules/detail-user/detail-user.module')
          .then(module => module.DetailUserModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
