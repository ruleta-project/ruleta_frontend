import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { faBars, faList, faUserCircle,faQuestionCircle,faSignOutAlt, faUser } from '@fortawesome/free-solid-svg-icons';
import { LoginService } from 'src/app/auth/login/login.service';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/auth/account.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.styl']
})
export class NavbarComponent implements OnInit {
  /*VARS*/
  nameUser:string;
  saldo: number;
  authorities:string[];
  //icons
  faBars = faBars;
  faGripLines = faList;
  faUserCircle = faUserCircle;
  faQuestionCircle = faQuestionCircle;
  faSignOutAlt = faSignOutAlt;
  faUser = faUser;
  //Switch
  isActive = false;
  //Events
  @Output() openEvent = new EventEmitter<boolean>();
//pido recibir un valor
  @Input() isActiveNavBar: boolean;
  //responsive design
  isMobileLayout:boolean
  constructor(
    private loginService: LoginService,
    private router: Router,
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    this.nameUser = this.accountService.getName();
    this.saldo = this.accountService.getSaldo();
    this.authorities = this.accountService.getRolUser();
  }

  changeEvent(): void {
    this.isActive = !this.isActive;
    this.openEvent.emit(this.isActive);
  }
  logout(): void {
    this.loginService.logout();
    this.router.navigate(["/"]);

  }
}
