import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Account } from './auth-shared/models/account.model';
import { tap, catchError, shareReplay } from 'rxjs/operators';
import { createRequestParams } from '../utils/request.utils';
import { IUser } from '../share/models/IUser';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;

  constructor(
    private http: HttpClient, private router: Router
  ) { }
//user/account
  create(detailUser: IUser): Observable<IUser> {
    return this.http.post<IUser>(`${environment.END_POINT}/api/user/account`, detailUser);
  }

  /**
   *
   * @param force Identify user in backend
   *
   */

  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache || force || !this.isAuthenticated()) {
      this.accountCache = this.fetch().pipe(catchError(() => {
        return of(null);
      }), tap((account: Account | null) => {
        this.authenticate(account);
        if (account) {
          this.router.navigate(['/index'])
        }
        else {
          this.router.navigate(['login'])

        }
      }), shareReplay());
    }

    return this.accountCache;
  }


  /*Para autenticar */
  authenticate(account: Account | null): void {
    this.userIdentity = account;
  }

  /*preguntar si esta autenticado */
  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }
  /*Obsevable => Pendiente si esta autenticado o No */
  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }
  getName(): string {
    return this.userIdentity.nombre;
  }
  getIdUser() : number {
    return this.userIdentity.id;
  }
  getRolUser(): string[] {
    return this.userIdentity.authorities;
  }

  getSaldo(): number {
    return this.userIdentity.saldo;
  }

  /**
 * @param authorities verify rols of users
 */
hasAnyAuthority(authorities: string[] | string): boolean {
  if (!this.userIdentity || !this.userIdentity.authorities) {
    return false;
  }
  if (!Array.isArray(authorities)) {
    authorities = [authorities];
  }
  return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
}

  /*SEND CREDENTIALS MAIN*/
  private fetch(): Observable<Account> {
    const jwt = localStorage.getItem('access_token') || sessionStorage.getItem('access_token');

    if (jwt) {
      const payload: any = JSON.parse(atob(jwt.split('.')[1]));
      const params = createRequestParams({ username: payload.user_name });
      return this.http.get<Account>(`${environment.END_POINT}/api/user/account`, { params: params });
    }

  }

}
