import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ICredentials } from './auth-shared/models/credentials';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  /**
   *
   * @param credentials
   * This method use for get token and send credentials of users
   */
  public login(credentials: ICredentials): Observable<any> {

    let params = new URLSearchParams();
    params.set('grant_type', credentials.grant_type);
    params.set('username', credentials.username);
    params.set('password', credentials.password);
    /*SEND CREDENTIALS MAIN*/
    const encoderApp = btoa('ruletaApp' + ':' + '12345');
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded', //Content-Type => tipo de formato en que se van a ir los datos
      'Authorization': 'Basic ' + encoderApp
    });
   /*observe:'response' => esta pendiete de las respuestas tanto de los headers y body */
    return this.http.post<any>(`${environment.END_POINT}/oauth/token`, params.toString(), { headers: httpHeaders, observe:'response' })
      .pipe(map(res => {
        /*SAVE TOKEN*/
        const jwt = res.body.access_token;
        this.storeAuthenticationToken(jwt, credentials.rememberMe);
      }));

  }

  public logout(): Observable<any> {
    return new Observable(observe => {
      localStorage.removeItem('access_token');
      sessionStorage.removeItem('access_token');
      observe.complete();
    })
  }
  /*SAVE TOKEN IN LOCAL STORAGE OR SESION STORAGE*/
  private storeAuthenticationToken(jwt: string, rememberMe: boolean): void {
    if (rememberMe) {
      localStorage.setItem('access_token', jwt);
    } else {
      sessionStorage.setItem('access_token', jwt);
    }
  }

}
