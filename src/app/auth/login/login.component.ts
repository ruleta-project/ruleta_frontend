import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ICredentials, Credentials } from '../auth-shared/models/credentials';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {

  statusSend:boolean = true;

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    rememberMe: false
  })

  constructor(

    private fb: FormBuilder,
    private loginService: LoginService
  ) {
    const ruleta = document.getElementById('containerRuleta')
    ruleta.className = 'd-none'
   }

  ngOnInit(): void {
    
    sessionStorage.removeItem("access_token");
    localStorage.removeItem("access_token");


  }

  login(): void {
    
    const credentials: ICredentials = new Credentials();
    this.statusSend = true;
    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;
    credentials.grant_type = 'password';

    this.loginService.login(credentials)
    .subscribe((res:any) => {
    },(error:any) => {
      this.statusSend = false;
    })
  }
}
