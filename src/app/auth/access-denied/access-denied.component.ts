import { Component, OnInit } from '@angular/core';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.styl']
})
export class AccessDeniedComponent implements OnInit {

  faExclamationTriangle = faExclamationTriangle;
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }


  }


