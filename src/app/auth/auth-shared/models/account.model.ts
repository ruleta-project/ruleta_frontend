export class Account {
    constructor (
        //public activated: boolean,
        public id:number,
        public username:string,
        public nombre:string,
        public estado:boolean,
        public saldo:number,
        public rolsUsers:string[],
        public authorities: string[],
    ) {}
}