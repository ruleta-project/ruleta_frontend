import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DetailUserService } from '../detail-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { IUser } from 'src/app/share/models/IUser';

@Component({
  selector: 'app-detail-user-update',
  templateUrl: './detail-user-update.component.html',
  styleUrls: ['./detail-user-update.component.styl']
})
export class DetailUserUpdateComponent implements OnInit {


  userList: IUser;
  userFormGroupUpdate: FormGroup;
  usersListUpdate: IUser[];
  userUpdate: IUser
  //form
  formBuilderRol: FormGroup
  userRolsUserForm: FormGroup;
  updateUser: IUser;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private detailUserService: DetailUserService,
    private activatedRouter: ActivatedRoute,
  ) {
    
      
    this.userFormGroupUpdate = this.formBuilder.group({
      username: ['', Validators.required],
      nombre: ['', Validators.required],
      saldo: ['', Validators.required],
      password: ['', Validators.required],
      rolsUsers: ['', Validators.required]
    });
  }

  ngOnInit() {
    let id = this.activatedRouter.snapshot.paramMap.get('id');
    this.detailUserService.getUserById((id))
    .subscribe(res => {
      this.updateUser = res;
      this.loadFormUser(res);
    });
    
  }

  

  private loadFormUser(userLoad: IUser){
    this.userFormGroupUpdate.patchValue({
      username: userLoad.username,
      nombre: userLoad.nombre,
      password: userLoad.password,
      saldo: userLoad.saldo,
      rolsUsers: userLoad.rolsUsers
    })
  }


  postUser() {

    const user = {
      id: this.updateUser.id,
      nombre: this.userFormGroupUpdate.value.nombre,
      username: this.userFormGroupUpdate.value.username,
      password: this.userFormGroupUpdate.value.password,
      saldo: this.userFormGroupUpdate.value.saldo,
      estado: true,
      rolsUsers: [
        {
          id: parseInt(this.userFormGroupUpdate.value.rolsUsers),
        }
      ]
    }
   

      this.detailUserService.updateUser(user)
    .subscribe( res =>{
      alert("Se Ha actualizado Correctamente")
      history.back()
      }, error => {
    });
    
      
  }
  

}