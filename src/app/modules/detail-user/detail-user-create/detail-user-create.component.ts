import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DetailUserService } from '../detail-user.service';
import { Router } from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { IUser } from 'src/app/share/models/IUser';

@Component({
  selector: 'app-detail-user-create',
  templateUrl: './detail-user-create.component.html',
  styleUrls: ['./detail-user-create.component.styl']
})

export class DetailUserCreateComponent{

  model: NgbDateStruct;


  //form
  userFormGroup: FormGroup;
  formBuilderRol: FormGroup
  userRolsUserForm: FormGroup;
  statusEnabled: true;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private detailUserService: DetailUserService,

  ) {
    this.userFormGroup = this.formBuilder.group({
      nombre: ['', Validators.required],
      username: ['', Validators.required],
      saldo: ['', Validators.required],
      password: ['', Validators.required],
      rolsUsers: ['', Validators.required]

    })
  }

  ngOnInit() {
  }



  postUser(): void {

    const user = { 
        nombre: this.userFormGroup.value.nombre,
        username: this.userFormGroup.value.username,
        password: this.userFormGroup.value.password,
        rolsUsers: [
            {
              id: parseInt(this.userFormGroup.value.rolsUsers),
            }
        ]
    }
    
    this.detailUserService.postUser(user)
      .subscribe( res => {
          history.back()
        }, error => {
          console.log(error);
      });

  }
  


}