
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { createRequestParams } from 'src/app/utils/request.utils';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IUser } from 'src/app/share/models/IUser';


@Injectable({
  providedIn: 'root'
})
export class DetailUserService {


  constructor(private http: HttpClient) { }

  public getUserById(id:any): Observable<IUser> {

    return this.http.get<IUser>(`${environment.END_POINT}/api/user/${id}`)
      .pipe(map(res => {
        return res;
      }));
    }


  // GET ALL USERS
  public getAllUsers(req?: any): Observable<IUser[]> {
    let params = createRequestParams(req);
    return this.http.get<IUser[]>(`${environment.END_POINT}/api/users`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  };


  public postUser(IUser:any): Observable<any> {
    return this.http.post<any>(`${environment.END_POINT}/api/user`, IUser)
      .pipe(map(res => {
        return res;
      }));
  }

  public updateUser(IUser:any): Observable<IUser>{
    return this.http.put<IUser>(`${environment.END_POINT}/api/user`, IUser)
    .pipe(map(res => {
      return res;
    }));
  }

  public deleteUser(id: string): Observable<IUser>{
    return this.http.delete<IUser>(`${environment.END_POINT}/api/user/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

  public getAccount(username: string): Observable<IUser>{
    const params = createRequestParams(username);
    return this.http.get<IUser>(`${environment.END_POINT}/api/user/account`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  public getDocumentNumber(documentNumber: string): Observable<IUser>{
    const params = createRequestParams(documentNumber);
    return this.http.get<IUser>(`${environment.END_POINT}/api/detail-user/search`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }


}