import { Component, OnInit } from '@angular/core';
import { DetailUserService } from '../detail-user.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IUser } from '../../../share/models/IUser';
import { faTrash, faPen , faEye} from '@fortawesome/free-solid-svg-icons'
@Component({
  selector: 'app-detail-user-list',
  templateUrl: './detail-user-list.component.html',
  styleUrls: ['./detail-user-list.component.styl']
})
export class DetailUserListComponent implements OnInit {


  //icons
  faTrash=faTrash;
  faPen=faPen;
  faEye=faEye;

  //PAGINACION Atributte
  pageSize = 5;
  pageNumber = 0;
  listPage = [];
  detailUserList: IUser[] = [];
  detailUserRes: IUser[];
 

  constructor(
    private detailUserService:DetailUserService,
    private fb : FormBuilder
  ) { 
    const ruleta = document.getElementById('containerRuleta')
    ruleta.className = 'd-none'
  }

  ngOnInit() {
    this.loadingPagination
    this.loadingPagination(this.pageNumber, this.pageSize);
     
   }
 // TRAER DATOS POR PAGINACIONS

   loadingPagination( pageNumber: number, pageSize: number): void {
    this.detailUserService.getAllUsers({
      'pageSize': pageSize,
      'pageNumber': pageNumber
    })
      .subscribe((res: any) => {
        this.detailUserList = res.content;
        this.formatPagination(res.totalPages);
      });
   }

   private formatPagination(totalPages: number): void{
     this.listPage = [];
       for(let i = 0; i < totalPages; i++){
         this.listPage.push(i);
       }
   }
    
  deleteUser(id: string){
    this.detailUserService.deleteUser(id)
    .subscribe(res => {
      this.ngOnInit();
    });
  }
}
