import { Component, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AccountService } from 'src/app/auth/account.service';
import { DetailUserService } from '../detail-user/detail-user.service';
import swal from 'sweetalert2';


// declare global {
//   interface Window { eleccionUsuario: any; }
// }

@Component({
  selector: 'app-ruleta',
  templateUrl: './ruleta.component.html',
  styleUrls: ['./ruleta.component.styl']
})


export class RuletaComponent implements OnInit {

  userData: any
  eleccion: any
  gameGroup: FormGroup;
  minValue: number
  maxValue: number
  todoOK:string

 
  constructor(private formBuilder: FormBuilder, private detailUserService: DetailUserService, private accountService: AccountService) {
  
    this.eleccion = {
      saldo: null,
      color: '', 
      confirm:false,
      allIn: false,
      devuelveInformacion: false,
      gano:false,
      apuntoVerde: false
    }

    // get object ruleta and className
    const ruleta = document.getElementById('containerRuleta')
    ruleta.className = 'd-block'
   }

  async ngOnInit() {
    

    let obj_user = JSON.parse(localStorage.getItem('eleccionUser'))
    // get USER BY ID
    this.userData = await this.getUserById()

    try {
      if (obj_user.devuelveInformacion) {
        this.onChange(obj_user)
        }
      
    }
    catch (err) {}
    
  
    // verifico el saldo del usuario entre los valores permitidos
    let max = (this.userData.saldo * 19) / 100
    let min = (this.userData.saldo * 11) / 100
    this.maxValue = max
    this.minValue = min
  }

  getUserById() : any {
    return new Promise((resolve, reject) => {
      this.detailUserService.getUserById(this.accountService.getIdUser())
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err)
        });
    })
   

  }

  onChange(obj_user) {
    const { gano, saldo, apuntoVerde } = obj_user

    if (gano) {
      if (apuntoVerde) {
        this.userData.saldo += saldo * 10

      } else {
        this.userData.saldo += saldo
      }
      
    } else {
      this.userData.saldo -=  saldo;
    }

    this.detailUserService.updateUser(this.userData)
      .subscribe(res => {
        swal.fire({
          icon: 'success',
          title: 'Saldo actualizado',
          text: `Se actualizo tu saldo, ahora es de : ${res.saldo}`,
        })
      }, error => {
        console.error(error)
      });
    localStorage.setItem('eleccionUser', JSON.stringify({}))
  }
  

  confirm() {
    this.eleccion.confirm = true

    if (!this.eleccion.saldo || !this.eleccion.color) {
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Llena todos los campos y confirma tu eleccion!!',
      })
    }
    // valor debe ser entre 11 y 19 porciento del valor que posee
    else if (this.eleccion.saldo > this.maxValue || this.eleccion.saldo < this.minValue) {
      swal.fire({
        icon: 'error',
        title: 'Hey!',
        text: 'Debes apostar entre un 11% y 19% de lo que tienes',
      })

    }
    else if (this.userData.saldo <= 1000) {
      this.eleccion.allIn = true
      swal.fire('Vas allIn!!');
    }

    else { 
      localStorage.setItem('eleccionUser', JSON.stringify(this.eleccion) )
      this.todoOK = " OK"
      setTimeout(() => {
        this.todoOK = ""
      }, 3000)
    }
    

  }

  

}
